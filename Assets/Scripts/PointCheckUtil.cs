﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Collections;

public class PointCheckUtil
{
	public static bool CheckIfPointInside(Vector3 point, LayerMask layerMask, float radius)
	{

		Vector3 goal = point + Random.onUnitSphere * radius;

		return((CountHitsInLine(point, goal, layerMask) + CountHitsInLine(goal, point, layerMask)) % 2 == 1);
	}

	public static bool CheckIfPointInside(Vector3 point, LayerMask layerMask, Bounds bounds)
	{

		float radius = bounds.size.magnitude;

		Vector3 goal = point + Random.onUnitSphere * radius;

		return ((CountHitsInLine(point, goal, layerMask) + CountHitsInLine(goal, point, layerMask)) % 2 == 1);
	}


	public static int CountHitsInLine(Vector3 startPosition, Vector3 endPosition, LayerMask layerMask)
	{
		int hits = 0;
		RaycastHit hitInfo;

		Vector3 pos = startPosition;
		Vector3 dir = (endPosition - startPosition).normalized;

		int tries = 0;

		while(pos != endPosition && tries < 32)
		{
			tries++;
			if (Physics.Linecast(pos, endPosition, out hitInfo, layerMask))
			{
				hits++;
				pos = hitInfo.point + dir*0.01f;
			}
			else
			{
				pos = endPosition;
			}
		}
		return hits;
	}

	// Sample sampleSize points within the bounds to see if they inside any colliders in the layerMask
	// Return the fraction of samples that are hits. Returns a range from 0.0f (none) to 1.0f (all)
	public static float CheckFills(Bounds bounds, LayerMask containerLayerMask, int sampleSize, float radius, Vector3[] samplePoints = null, bool[] hitResult = null)
	{
		int pointsInside = 0;
		var raycastHits = new NativeArray<RaycastHit>(sampleSize * 2, Allocator.Temp);
		var commands = new NativeArray<RaycastCommand>(sampleSize * 2, Allocator.Temp);
		var hitCount = new NativeArray<int>(sampleSize, Allocator.Temp);

		Vector3 boundsMin = bounds.min;
		Vector3 boundsMax = bounds.max;

		for (int i = 0; i < sampleSize; i++)
		{
			Vector3 startPoint = (samplePoints == null) ? GetRandomPointInBounds(bounds) : samplePoints[i];
			Vector3 directionToEndPoint = Random.onUnitSphere;
			Vector3 endPoint = startPoint + directionToEndPoint * radius;
			Vector3 directionToStartPoint = -directionToEndPoint;

			commands[i * 2] = new RaycastCommand(startPoint, directionToEndPoint, radius, containerLayerMask, 1);
			commands[i * 2 + 1] = new RaycastCommand(endPoint, directionToStartPoint, radius, containerLayerMask, 1);
		}

		bool hasHits = true;
		int tries = 0;

		// iterate through and count up the hits
		do
		{
			var batch = RaycastCommand.ScheduleBatch(commands, raycastHits, 16);
			batch.Complete();

			hasHits = false;

			for (int i = 0; i < sampleSize; i++)
			{
				RaycastHit outHit = raycastHits[i * 2];
				RaycastCommand outCommand = commands[i * 2];

				if (outCommand.distance == 0.0f)
				{
					// skip
				}
				// end of ray, minimize distance
				else if (outHit.collider == null)
				{
					outCommand.from = outCommand.from + outCommand.direction * outCommand.distance;
					outCommand.distance = 0.0f;
				}
				// there is a hit, propagate the start point to a bit right after the hit
				else
				{
					hasHits = true;
					hitCount[i] = hitCount[i]-1;
					outCommand.distance -= outHit.distance + 0.001f;
					outCommand.from = outHit.point + outCommand.direction * 0.001f;
				}
				commands[i * 2] = outCommand;

				RaycastHit inHit = raycastHits[i * 2 + 1];
				RaycastCommand inCommand = commands[i * 2 + 1];

				if (inCommand.distance == 0.0f)
				{
					// skip
				}
				else if (inHit.collider == null)
				{
					inCommand.from = inCommand.from + inCommand.direction * inCommand.distance;
					inCommand.distance = 0.0f;
				}
				else
				{
					hasHits = true;
					hitCount[i] = hitCount[i] + 1;
					inCommand.distance -= inHit.distance + 0.001f;
					inCommand.from = inHit.point + inCommand.direction * 0.001f;
				}
				commands[i * 2 + 1] = inCommand;
			}
			tries++;

		} while (hasHits);

		// loop through and find how many insides there are
		for(int i = 0; i < sampleSize; i++)
		{
			bool hit = hitCount[i] > 0;

			if (hit)
			{
				pointsInside += 1;
			}

			if (hitResult != null)
			{
				hitResult[i] = hit;
			}
		}

		commands.Dispose();
		raycastHits.Dispose();
		hitCount.Dispose();

		return ((float) pointsInside) / ((float) sampleSize);
	}

	public static Vector3 GetRandomPointInBounds(Bounds bounds)
	{
		Vector3 boundsMin = bounds.min;
		Vector3 boundsMax = bounds.max;

		return (new Vector3(
				Random.Range(boundsMin.x, boundsMax.x),
				Random.Range(boundsMin.y, boundsMax.y),
				Random.Range(boundsMin.z, boundsMax.z)
				));
	}
	public static float CompareFill(Bounds bounds, LayerMask containerLayerMask, LayerMask fillLayerMask, int sampleSize, float maxDistance, Vector3[] samplePoints = null)
	{
		var raycastHits = new NativeArray<RaycastHit>(sampleSize * 4, Allocator.Temp);
		var commands = new NativeArray<RaycastCommand>(sampleSize * 4, Allocator.Temp);
		var hitCount = new NativeArray<int>(sampleSize*2, Allocator.Temp);

		for (int i = 0; i < sampleSize; i++)
		{
			Vector3 startPoint = (samplePoints == null) ? GetRandomPointInBounds(bounds) : samplePoints[i];
			Vector3 directionToEndPoint = Random.onUnitSphere;
			Vector3 endPoint = startPoint + directionToEndPoint * maxDistance;
			Vector3 directionToStartPoint = -directionToEndPoint;

			int baseIndex = i * 4;
			commands[baseIndex] = new RaycastCommand(startPoint, directionToEndPoint, maxDistance, containerLayerMask, 1);
			commands[baseIndex + 1] = new RaycastCommand(endPoint, directionToStartPoint, maxDistance, containerLayerMask, 1);
			commands[baseIndex + 2] = new RaycastCommand(startPoint, directionToEndPoint, maxDistance, fillLayerMask, 1);
			commands[baseIndex + 3] = new RaycastCommand(endPoint, directionToStartPoint, maxDistance, fillLayerMask, 1);
		}

		for(int i = 0; i < sampleSize; i++)
		{
			hitCount[i*2] = 0;
			hitCount[i * 2 + 1] = 0;
		}

		bool hasHits = true;
		int tries = 0;

		// iterate through and count up the hits
		do
		{
			var batch = RaycastCommand.ScheduleBatch(commands, raycastHits, 32);
			batch.Complete();

			hasHits = false;

			for (int i = 0; i < sampleSize; i++)
			{
				int baseIndex = i * 4; // for the raycast commands and raycast hits
				int hitIndex = i * 2; // for the hitCount

				// rays going outwards
				RaycastHit outHit = raycastHits[baseIndex];
				RaycastCommand outCommand = commands[baseIndex];

				if (outCommand.distance == 0.0f)
				{
					// skip
				}
				// end of ray, minimize distance
				else if (outHit.collider == null)
				{
					outCommand.from = outCommand.from + outCommand.direction * outCommand.distance;
					outCommand.distance = 0.0f;
				}
				// there is a hit, propagate the start point to a bit right after the hit
				else
				{
					hasHits = true;
					hitCount[hitIndex] = hitCount[hitIndex] - 1;
					outCommand.distance -= outHit.distance + 0.001f;
					outCommand.from = outHit.point + outCommand.direction * 0.001f;
				}

				commands[baseIndex] = outCommand;

				baseIndex++;

				// rays coming inwards
				RaycastHit inHit = raycastHits[baseIndex];
				RaycastCommand inCommand = commands[baseIndex];

				if (inCommand.distance == 0.0f)
				{
					// skip
				}
				else if (inHit.collider == null)
				{
					inCommand.from = inCommand.from + inCommand.direction * inCommand.distance;
					inCommand.distance = 0.0f;
				}
				else
				{
					hasHits = true;
					hitCount[hitIndex] = hitCount[hitIndex] + 1;
					inCommand.distance -= inHit.distance + 0.001f;
					inCommand.from = inHit.point + inCommand.direction * 0.001f;
				}
				commands[baseIndex] = inCommand;


				// fill, rays going out
				baseIndex++;
				hitIndex++;

				outHit = raycastHits[baseIndex];
				outCommand = commands[baseIndex];

				if (outCommand.distance == 0.0f)
				{
					// skip
				}
				// end of ray, minimize distance
				else if (outHit.collider == null)
				{
					outCommand.from = outCommand.from + outCommand.direction * outCommand.distance;
					outCommand.distance = 0.0f;
				}
				// there is a hit, propagate the start point to a bit right after the hit
				else
				{
					hasHits = true;
					hitCount[hitIndex] = hitCount[hitIndex] - 1;
					outCommand.distance -= outHit.distance + 0.001f;
					outCommand.from = outHit.point + outCommand.direction * 0.001f;
				}

				commands[baseIndex] = outCommand;

				// fill, rays coming in

				baseIndex++;

				inHit = raycastHits[baseIndex];
				inCommand = commands[baseIndex];

				if (inCommand.distance == 0.0f)
				{
					// skip
				}
				else if (inHit.collider == null)
				{
					inCommand.from = inCommand.from + inCommand.direction * inCommand.distance;
					inCommand.distance = 0.0f;
				}
				else
				{
					hasHits = true;
					hitCount[hitIndex] = hitCount[hitIndex] + 1;
					inCommand.distance -= inHit.distance + 0.001f;
					inCommand.from = inHit.point + inCommand.direction * 0.001f;
				}
				commands[baseIndex] = inCommand;
			}
			tries++;

		} while (hasHits);

		int matches = 0;

		// loop through and find how many insides there are
		for (int i = 0; i < sampleSize; i++)
		{
			bool containerHit = hitCount[i*2] > 0;
			bool fillHit = hitCount[i*2+1] > 0;
			matches += containerHit == fillHit ? 1 : 0;
		}

		commands.Dispose();
		raycastHits.Dispose();
		hitCount.Dispose();
		return ((float)matches) / ((float)sampleSize);
	}
}
