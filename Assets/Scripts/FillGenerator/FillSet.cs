﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillSet : MonoBehaviour {
	public List<Fill> fills = new List<Fill>();
	public float score;
	public float spinner;
	
	public void PopulateRandomFillSet(Bounds bounds, Stack<Fill>[] fillPools, int fillCount)
	{
		for(int i = 0; i < fillCount; i++)
		{
			AddFill(bounds, fillPools);
		}
	}

	public FillSet Breed(FillSet fs1, FillSet fs2, Stack<Fill>[] fillPools)
	{
		int fillCount = fs1.fills.Count;


		for (int i = 0; i < fillCount; i++)
		{
			// recycle the original first fill
			Fill oldFill = fills[i];
			//oldFill.gameObject.SetActive(false);
			//oldFill.transform.parent = null;
			//fillPools[oldFill.fillPoolIndex].Push(oldFill);

			// copy the second parent's over to the first
			Fill fill = Random.value <= 0.5f ? fs1.fills[i] : fs2.fills[i];
			//Fill newFill = fillPools[fill.fillPoolIndex].Pop();
			Fill newFill = oldFill;

			//newFill.transform.SetParent(transform);
			newFill.transform.localPosition = fill.transform.localPosition;
			newFill.transform.localRotation = fill.transform.localRotation;
			newFill.transform.localScale = fill.transform.localScale;
			//newFill.gameObject.SetActive(true);
			//fills[i] = newFill;
		}

		//transform.parent = fs1.transform.parent;
		return this;
	}

	// strip of beeding
	public FillSet Breed2(FillSet fs1, FillSet fs2, Stack<Fill>[] fillPools)
	{
		int fillCount = fs1.fills.Count;
		int startFillIndex = Random.Range(0, fillCount);
		int endFillIndex = Random.Range(0, fillCount) % fillCount;
		
		for(int i = 0; i < fillCount; i++)
		{
			Fill oldFill = fills[i];

			// copy the second parent's over to the first
			Fill fill = (
				i >= startFillIndex && i <= endFillIndex ||
				endFillIndex < startFillIndex && (i <= endFillIndex || i >= startFillIndex)
				) ? fs1.fills[i] : fs2.fills[i];
			Fill newFill = oldFill;
			
			newFill.transform.localPosition = fill.transform.localPosition;
			newFill.transform.localRotation = fill.transform.localRotation;
			newFill.transform.localScale = fill.transform.localScale;
		}
		
		//transform.parent = fs1.transform.parent;
		return this;
	}

	public void Constrain(Bounds bounds)
	{
		Vector3 min = bounds.min;
		Vector3 max = bounds.max;


		for (int i = 0; i < fills.Count; i++)
		{
			Fill fill = fills[i];
			Bounds fillBounds = fill.myCollider.bounds;
			if (!fillBounds.Intersects(bounds))
			{
				Vector3 pos = new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
				fill.transform.position = pos;
			}
		}
	}

	public void Constrain(Collider[] colliders, Bounds overallBounds)
	{
		for (int i = 0; i < fills.Count; i++)
		{
			bool isConstrained = false;
			Fill fill = fills[i];
			Bounds fillBounds = fill.myCollider.bounds;

			for (int j = 0; j < colliders.Length; j++)
			{
				Bounds bounds = colliders[j].bounds;
				Bounds boundsWidened = colliders[j].bounds;
				boundsWidened.extents += Vector3.one * 0.5f;
				//if (bounds.Intersects(fillBounds) && boundsWidened.Contains(fillBounds.min) && boundsWidened.Contains(fillBounds.max))

				if (bounds.Intersects(fillBounds))
				{
					isConstrained = true;
					break;
				}
			}

			if (!isConstrained)
			{
				Vector3 min = overallBounds.min;
				Vector3 max = overallBounds.max;

				Vector3 pos = new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
				fill.transform.position = pos;
			}
		}
	}

	public void Mutate(float positionMutationRate, float rotationMutationRate, float scaleMutationRate, float totallyRandomMutationRate, Bounds overallBounds, bool mutateOnlyOne = false)
	{
		Vector3 min = overallBounds.min;
		Vector3 max = overallBounds.max;

		if (mutateOnlyOne)
		{
			Fill fill = fills[Random.Range(0,fills.Count)];
			MutateFill(positionMutationRate, rotationMutationRate, scaleMutationRate, totallyRandomMutationRate, min, max, fill);
		}
		else
		{
			for (int i = 0; i < fills.Count; i++)
			{
				Fill fill = fills[i];
				MutateFill(positionMutationRate, rotationMutationRate, scaleMutationRate, totallyRandomMutationRate, min, max, fill);
			}
		}
	}

	private static void MutateFill(float positionMutationRate, float rotationMutationRate, float scaleMutationRate, float totallyRandomMutationRate, Vector3 min, Vector3 max, Fill fill)
	{
		Transform fillTransform = fill.transform;
		fillTransform.position += Quaternion.LookRotation(Random.onUnitSphere) * Vector3.forward * positionMutationRate * Random.value;
		fillTransform.localRotation = Quaternion.Slerp(fillTransform.localRotation, Quaternion.LookRotation(Random.onUnitSphere), rotationMutationRate * Random.value);
		Vector3 scale = fillTransform.localScale;

		if (fill.uniformScale)
		{
			scale *= (1.0f + Random.Range(-scaleMutationRate, scaleMutationRate));
		}
		else
		{
			scale = Vector3.Scale(scale, new Vector3((1.0f + Random.Range(-scaleMutationRate, scaleMutationRate)),
				(1.0f + Random.Range(-scaleMutationRate, scaleMutationRate)),
				(1.0f + Random.Range(-scaleMutationRate, scaleMutationRate)))
			);
		}

		scale = Vector3.Max(fillTransform.localScale, fill.minScale);
		scale = Vector3.Min(fillTransform.localScale, fill.maxScale);
		fillTransform.localScale = scale;

		if (Random.value < totallyRandomMutationRate)
		{
			BounceFill(min, max, fill, fillTransform);
		}
	}

	public void Mutate2(float positionMutationRate, float rotationMutationRate, float scaleMutationRate, float totallyRandomMutationRate, Bounds overallBounds, bool mutateOnlyOne = false)
	{
		Vector3 min = overallBounds.min;
		Vector3 max = overallBounds.max;

		if (mutateOnlyOne)
		{
			Fill fill = fills[Random.Range(0, fills.Count)];
			MutateFill2(positionMutationRate, rotationMutationRate, scaleMutationRate, totallyRandomMutationRate, min, max, fill);
		}
		else
		{
			for (int i = 0; i < fills.Count; i++)
			{
				Fill fill = fills[i];
				MutateFill2(positionMutationRate, rotationMutationRate, scaleMutationRate, totallyRandomMutationRate, min, max, fill);
			}
		}
	}

	private static void MutateFill2(float positionMutationRate, float rotationMutationRate, float scaleMutationRate, float totallyRandomMutationRate, Vector3 min, Vector3 max, Fill fill)
	{
		Transform fillTransform = fill.transform;

		if (Random.value < positionMutationRate)
		{
			Vector3 pos = new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
			fillTransform.position = pos;
		}

		if (Random.value < rotationMutationRate)
		{
			fill.RandomizeRotation();
		}

		if (Random.value < scaleMutationRate)
		{
			fill.RandomizeScale();
		}

		if (Random.value < totallyRandomMutationRate)
		{
			BounceFill(min, max, fill, fillTransform);
		}
	}

	public static void BounceFill(Vector3 boundsMin, Vector3 boundsMax, Fill fill, Transform fillTransform = null)
	{
		fillTransform = (fillTransform == null) ? fill.transform : fillTransform;
		
		Vector3 pos = new Vector3(Random.Range(boundsMin.x, boundsMax.x), Random.Range(boundsMin.y, boundsMax.y), Random.Range(boundsMin.z, boundsMax.z));
		fillTransform.position = pos;
		fill.RandomizeScale();
		fill.RandomizeRotation();
	}

	public void ShuffleFills()
	{
		for(int i = 0; i < fills.Count; i++)
		{
			int otherIndex = Random.Range(i, fills.Count-1);
			Fill tempFill = fills[i];
			fills[i] = fills[otherIndex];
			fills[otherIndex] = tempFill;
		}
	}
	public Fill AddFill(Bounds bounds, Stack<Fill>[] fillPools)
	{
		Vector3 min = bounds.min;
		Vector3 max = bounds.max;
		
		Vector3 pos = new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
		Stack<Fill> pool = fillPools[Random.Range(0, fillPools.Length)];
		
		Fill fill = pool.Pop();
		fill.transform.parent = null;
		fill.transform.position = pos;
		
		if (fill.canHaveRandomRotation)
		{
			fill.RandomizeRotation();
		}
		fill.RandomizeScale();
		
		fill.transform.parent = transform;
		fill.gameObject.SetActive(true);
		fills.Add(fill);
		return fill;
	}
	
	public Bounds CalculateOverallBounds()
	{
		Bounds result = fills[0].myCollider.bounds;
		
		for(int i = 1; i < fills.Count; i++)
		{
			Bounds b2 = fills[i].myCollider.bounds;
			result.Encapsulate(b2);	
		}
		
		return result;
	}
	
	public bool ColliderBoundCheck(Vector3 pos)
	{
		bool result = false;
		for(int i = 0; i < fills.Count; i++)
		{
			if (fills[i].myCollider.bounds.Contains(pos))
			{
				result = true;
				break;
			}
		}
		
		return result;
	}

	public void SetFillActivation(bool active)
	{
		for (int i = 0; i < fills.Count; i++)
		{
			Fill fill = fills[i];
			if (fill == null) continue;

			fill.gameObject.SetActive(active);
		}
	}
}
