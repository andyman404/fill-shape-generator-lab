﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fill : MonoBehaviour {
	public Vector3 minScale;
	public Vector3 maxScale;
	public bool uniformScale = false;
	public bool canHaveRandomRotation = true;
	public Collider myCollider;
	
	public Vector3 minRotation;
	public Vector3 maxRotation;
	
	public int fillPoolIndex = 0;
	public Rigidbody rb;

	public void RandomizeScale()
	{
		if (uniformScale)
		{
			float scaleSize = Random.Range(minScale.x, maxScale.x);
			transform.localScale = Vector3.one * scaleSize;
		}
		else
		{
			transform.localScale = new Vector3(
				Random.Range(minScale.x, maxScale.x),
				Random.Range(minScale.y, maxScale.y),
				Random.Range(minScale.z, maxScale.z)
			);
		}
	}
	
	public void RandomizeRotation()
	{
		transform.localRotation *= Quaternion.Euler(
			Random.Range(minRotation.x, maxRotation.x),
			Random.Range(minRotation.y, maxRotation.y),
			Random.Range(minRotation.z, maxRotation.z)
		);
	}
	
}
