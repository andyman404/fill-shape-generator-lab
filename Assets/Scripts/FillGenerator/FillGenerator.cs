﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.UI;

public class FillGenerator : MonoBehaviour
{
	public enum RaycastOriginShape { Ellipsoid = 0, Box = 1, Random = 2 };
	public enum ScoringType { Raycast = 0, Volumetric = 1, Random = 2 };

	[Header("Population")]

	[Tooltip("How many configurations in the population")]
	[Range(1.0f, 10000.0f)]
	public int populationSize = 100;

	[Tooltip("The number of fills each individual configuration is composed of")]
	[Range(1.0f, 1000.0f)]
	public int fillsPerSet = 50;

	[Header("____________________")]
	[Header("Prefabs")]
	public Fill[] fillPrefabs;
	public FillSet fillSetPrefab;
	public Pusher pusherPrefab;

	[Header("____________________")]
	[Header("Container")]
	public Transform targetContainer;
	private Collider[] colliders;

	[Header("____________________")]
	[Header("Layers")]
	public LayerMask containerLayerMask;
	public LayerMask fillLayerMask;

	[Header("____________________")]
	[Header("Running")]
	public int generationsPerTap = 10000;
	public int runToGeneration = 10000;

	[Header("____________________")]
	[Header("Mutation")]
	public float startMutationMultiplier = 1.0f;
	public float minMutationMultiplier = 0.1f;
	public float positionMutationRate = 0.05f;
	public float rotationMutationRate = 0.1f;
	public float scaleMutationRate = 0.1f;
	public float totallyRandomMutationRate = 0.01f;

	public bool shouldConstrain = true;
	public Vector3 constrainMargins = Vector3.zero;

	public bool shouldBounceOrphans = true;
	public int bounceEveryNGenerations = 10;
	public int orphanCheckSampleCount = 256;
	[Tooltip("If the orphan check returns something lower than this value, then bounce this fill.")]
	public float orphanBounceMax = 0.1f;

	[Header("____________________")]
	[Header("Breeding")]
	public bool useBestForParent0 = true;
	public bool useBestForParent1 = true;
	public bool swapGeneStripsWhenBreeding = true;

	[Header("____________________")]
	[Header("Scoring")]
	public ScoringType scoringType = ScoringType.Raycast;
	public int sampleCount = 1024;

	public float raycastGoodEnoughRadius = 1f;
	public bool raycastShrinkingGoodEnoughRadius = true;
	public RaycastOriginShape raycastOriginShape = RaycastOriginShape.Ellipsoid;

	public bool volumetricUsePushers = true;
	public int volumetricPushersUpdateEveryNGenerations = 10;
	public bool volumetricMultiplyPusherRadiusByMutationMultiplier = false;
	public float volumetricPusherRadius = 0.5f;
	public float volumetricPusherMass = 1.0f;
	public float volumetricPusherDrag = 100.0f;
	public float volumetricPusherAngularDrag = 100.0f;
	public bool volumetricPusherIsKinematic = false;

	[Header("____________________")]
	[Header("Debug")]
	public float mutationMultiplier = 1.0f;
	public int generations = 0;
	public FillSet bestFillSet;
	public float bestFillSetScore;
	public float bestScorePercentage;
	public float cumulativeScore = 0.0f;
	public float cumulativeFitness = 0.0f;

	[Header("____________________")]
	[Header("Interactions")]
	public bool bounceTriggered;
	public bool mutationIncrementTriggered;
	public bool mutationDecrementTriggered;

	public bool mutationDoubleTriggered;
	public bool mutationHalveTriggered;

	// private
	private Dictionary<Vector3, bool> containerPointCache;
	private Stack<Fill>[] fillPools;
	private float maxPossibleScore = 1f;

	private FillSet[] fillSets;
	private FillSet[] nextFillSets;
	private Bounds overallBounds;
	private Pusher[] pushers;

	public void TriggerBounce()
	{
		bounceTriggered = true;
	}

	public void TriggerMutationIncrement()
	{
		mutationIncrementTriggered = true;
	}
	public void TriggerMutationDecrement()
	{
		mutationDecrementTriggered = true;
	}
	public void TriggerMutationDouble()
	{
		mutationDoubleTriggered = true;
	}
	public void TriggerMutationHalve()
	{
		mutationHalveTriggered = true;
	}

	// Use this for initialization
	void Start()
	{
		mutationMultiplier = startMutationMultiplier;

		colliders = targetContainer.GetComponentsInChildren<Collider>();

		InitFillsPools();
		GenerateRandomFills();
		InitPushers();

		bestFillSet = ScoreAllFillSets();
		bestFillSetScore = bestFillSet.score;
		bestFillSet.gameObject.SetActive(true);
	}

	public void InitPushers()
	{
		pushers = new Pusher[sampleCount];

		for (int i = 0; i < sampleCount; i++)
		{
			Pusher pusher = Instantiate<Pusher>(pusherPrefab);
			pusher.gameObject.SetActive(false);
			pusher.sphereCollider.radius = volumetricPusherRadius;
			pusher.transform.parent = transform;
			Rigidbody pusherRigidbody = pusher.rb;
			if (pusherRigidbody != null)
			{
				pusherRigidbody.mass = volumetricPusherMass;
				pusherRigidbody.drag = volumetricPusherDrag;
				pusherRigidbody.angularDrag = volumetricPusherAngularDrag;
				pusherRigidbody.isKinematic = volumetricPusherIsKinematic;
			}
			pushers[i] = pusher;
		}
	}

	public void InitFillsPools()
	{
		int poolSize = fillsPerSet * populationSize * 2;

		fillPools = new Stack<Fill>[fillPrefabs.Length];

		for (int i = 0; i < fillPrefabs.Length; i++)
		{
			Stack<Fill> pool = new Stack<Fill>();
			for (int j = 0; j < poolSize; j++)
			{
				Fill fill = Instantiate<Fill>(fillPrefabs[i]);
				fill.gameObject.SetActive(false);
				fill.fillPoolIndex = i;
				pool.Push(fill);
			}
			fillPools[i] = pool;
		}
	}

	// Update is called once per frame
	void Update()
	{
		maxPossibleScore = 1.0f;

		if (generations < runToGeneration || runToGeneration < 0)
		{
			if (bestFillSet != null)
			{
				bestFillSet.gameObject.SetActive(false);
			}

			if (cumulativeScore != 0.0f)
			{
				Breed();
				if (mutationIncrementTriggered)
				{
					mutationMultiplier *= 1.25f;
					mutationIncrementTriggered = false;
				}
				if (mutationDoubleTriggered)
				{
					mutationMultiplier *= 2.0f;
					mutationDoubleTriggered = false;
				}
				if (mutationDecrementTriggered)
				{
					mutationMultiplier *= 0.75f;
					mutationDecrementTriggered = false;
				}
				if (mutationHalveTriggered)
				{
					mutationMultiplier *= 0.5f;
					mutationHalveTriggered = false;
				}


				Mutate();
				if (shouldBounceOrphans && generations % bounceEveryNGenerations == 0 || bounceTriggered)
				{
					bounceTriggered = false;
					BounceOrphans();
				}
				if (shouldConstrain)
				{
					Constrain();
				}
			}

			Score();
		}
	}


	private void Score()
	{
		float oldCumulativeScore = cumulativeScore;

		bestFillSet = ScoreAllFillSets();
		bestFillSetScore = bestFillSet.score;

		cumulativeFitness = cumulativeScore / ((float)populationSize);

		if (oldCumulativeScore < cumulativeScore)
		{
			mutationMultiplier *= 0.999f;
		}
		else
		{
			mutationMultiplier *= 1.001f;
		}
		mutationMultiplier *= 0.999f;

		mutationMultiplier = Mathf.Max(minMutationMultiplier, mutationMultiplier);
		bestFillSet.gameObject.SetActive(true);
		generations++;
	}

	public void Constrain()
	{
		Bounds bounds = FindOverallBoundingBox();

		// for all fill sets, if a fill is outside of the bounds, then respawn
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			fillSet.Constrain(colliders, bounds);
		}
	}

	public void Mutate()
	{
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			fillSet.Mutate(positionMutationRate * mutationMultiplier, rotationMutationRate * mutationMultiplier, scaleMutationRate * mutationMultiplier, totallyRandomMutationRate * mutationMultiplier, overallBounds);
		}
	}

	// fillsets should start out inactive, and will return to inactive
	public void BounceOrphans()
	{
		FindOverallBoundingBox();
		float maxRayDistance = overallBounds.size.magnitude * 2.0f;

		for(int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			if (fillSet == null) continue;

			fillSet.gameObject.SetActive(true);

			BounceOrphansForFillSet(fillSet, maxRayDistance);

			fillSet.gameObject.SetActive(false);
		}
	}

	// bounce any fills that are not intersecting with anything
	// other fillsets should be deactivated already
	public void BounceOrphansForFillSet(FillSet fillSet, float maxRayDistance)
	{
		int fillSetCount = fillSet.fills.Count;

		// first deactivate all fills
		fillSet.SetFillActivation(false);

		// do the test for each fill
		for(int i = 0; i < fillSetCount; i++)
		{
			// temporarily activate fill
			Fill fill = fillSet.fills[i];
			if (fill == null) continue;

			fill.gameObject.SetActive(true);

			bool bounced = false;
			int tries = 0;
			do
			{
				// do a fill check to see what commonalities it has with the container
				Bounds fillBounds = fill.myCollider.bounds;
				float containerFilled = PointCheckUtil.CheckFills(fillBounds, containerLayerMask, orphanCheckSampleCount, maxRayDistance);

				// if it is too low, then bounce it
				if (containerFilled <= orphanBounceMax)
				{
					FillSet.BounceFill(overallBounds.min, overallBounds.max, fill);
					bounced = true;
				}
				else
				{
					bounced = false;
				}
				tries++;
			} while (bounced && tries < 32);

			// deactivate fill
			fill.gameObject.SetActive(false);
		}

		// reactivate the fills
		fillSet.SetFillActivation(true);
	}


	public void Breed()
	{
		// for each individual in the population
		for (int i = 0; i < populationSize; i++)
		{
			FillSet parent0 = useBestForParent0 ? bestFillSet : PickWeightedRandomFillSet();
			FillSet parent1 = useBestForParent1 ? bestFillSet : PickWeightedRandomFillSet();

			nextFillSets[i] = swapGeneStripsWhenBreeding ? nextFillSets[i].Breed2(parent0, parent1, fillPools) : nextFillSets[i].Breed(parent0, parent1, fillPools);
		}

		// swap next fill set and fill set
		FillSet[] tempFillSets = fillSets;
		fillSets = nextFillSets;
		nextFillSets = tempFillSets;
	}

	public FillSet PickWeightedRandomFillSet()
	{
		float spinner = Random.Range(0.0f, cumulativeScore);
		FillSet result = fillSets[0];

		for (int i = 1; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			if (spinner <= fillSet.spinner)
			{
				result = fillSet;
				break;
			}
		}
		return result;
	}

	public void GenerateRandomFills()
	{
		FindOverallBoundingBox();

		fillSets = new FillSet[populationSize];
		nextFillSets = new FillSet[populationSize];

		// generate for fillsets
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = Instantiate(fillSetPrefab, transform.position, Quaternion.identity);
			fillSet.PopulateRandomFillSet(overallBounds, fillPools, fillsPerSet);

			fillSet.transform.SetParent(transform);
			fillSets[i] = fillSet;
			fillSets[i].gameObject.SetActive(false);
		}

		// generate for nextFillSets
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = Instantiate(fillSetPrefab, transform.position, Quaternion.identity);
			fillSet.PopulateRandomFillSet(overallBounds, fillPools, fillsPerSet);

			fillSet.transform.SetParent(transform);
			nextFillSets[i] = fillSet;
			nextFillSets[i].gameObject.SetActive(false);
		}
	}

	private Ray GetRandomBoxRay(Bounds bounds)
	{
		Vector3 min = bounds.min;
		Vector3 max = bounds.max;

		Vector3 origin = new Vector3(
			Random.Range(min.x, max.x),
			Random.Range(min.y, max.y),
			Random.Range(min.z, max.z)
			);

		Vector3 direction = Vector3.zero;

		switch (Random.Range(0, 6))
		{
			case 0:
				direction = Vector3.forward;
				origin.z = overallBounds.min.z;
				break;

			case 1:
				direction = Vector3.back;
				origin.z = overallBounds.max.z;
				break;

			case 2:
				direction = Vector3.left;
				origin.x = overallBounds.min.x;
				break;

			case 3:
				direction = Vector3.right;
				origin.x = overallBounds.max.x;
				break;

			case 4:
				direction = Vector3.down;
				origin.y = overallBounds.max.y;
				break;

			case 5:
				direction = Vector3.up;
				origin.y = overallBounds.min.y;
				break;
		}

		return new Ray(origin, direction);
	}

	private Ray GetEllipsoidRay(Vector3 center, Vector3 radius)
	{
		// pick from a sphere around the bounding box
		Vector3 direction = Random.onUnitSphere;
		Vector3 from = Vector3.Scale(-direction, radius) + center;
		direction = Vector3.Lerp(direction, Random.onUnitSphere, Random.value * 0.5f).normalized;

		return new Ray(from, direction);
	}

	public FillSet ScoreAllFillSets()
	{
		FillSet result = null;

		switch(scoringType)
		{
			case ScoringType.Raycast:
				result = ScoreAllFillSetsRaycast();
				break;
			case ScoringType.Volumetric:
				result = ScoreAllFillSetsVolumetric();
				break;
		}

		return result;
	}

	private Vector3[] samplePoints;
	private bool[] containerHits;
	private bool[] fillHits;

	public FillSet ScoreAllFillSetsVolumetric()
	{
		float bestScore = Mathf.NegativeInfinity;
		FillSet best = null;

		Bounds checkBounds = FindOverallBoundingBox();
		checkBounds.SetMinMax(overallBounds.min - constrainMargins, overallBounds.max + constrainMargins);

		float maxDistance = FindOverallBoundingBoxWithFillSets().size.magnitude;

		// populate sample points
		if (samplePoints == null)
		{
			samplePoints = new Vector3[sampleCount];
		}
		for (int i = 0; i < sampleCount; i++)
		{
			samplePoints[i] = PointCheckUtil.GetRandomPointInBounds(checkBounds);
		}

		// check container for hits
		if (containerHits == null)
		{
			containerHits = new bool[sampleCount];
			fillHits = new bool[sampleCount];
		}

		PointCheckUtil.CheckFills(checkBounds, containerLayerMask, sampleCount, maxDistance, samplePoints, containerHits);

		// check each fillset and compare results with the container's hits
		cumulativeScore = 0.0f;

		// loop through fillSets, and activate one by one
		// and do the raycasts
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			fillSet.gameObject.SetActive(true);

			PointCheckUtil.CheckFills(checkBounds, fillLayerMask, sampleCount, maxDistance, samplePoints, fillHits);

			float score = 0.0f;

			// compare container and fill to come up with the score
			for (int hitIndex = 0; hitIndex < sampleCount; hitIndex++)
			{
				score += (containerHits[hitIndex] == fillHits[hitIndex]) ? 1.0f : 0.0f;
			}
			score = score / ((float)sampleCount);

			cumulativeScore += score;
			fillSet.score = score;
			fillSet.spinner = cumulativeScore;
			fillSet.gameObject.SetActive(false);

			if (fillSet.score > bestScore)
			{
				best = fillSet;
				bestScore = fillSet.score;
			}
		}

		if (generations % volumetricPushersUpdateEveryNGenerations == 0)
		{
			UpdatePushers();
		}

		return best;
	}


	private void UpdatePushers()
	{
		bool usePushers = (scoringType == ScoringType.Volumetric) && volumetricUsePushers;
		float pushRadius = volumetricMultiplyPusherRadiusByMutationMultiplier ? (mutationMultiplier * volumetricPusherRadius) : volumetricPusherRadius;
		for (int i = 0; i < sampleCount; i++)
		{
			Pusher pusher = pushers[i];
			pusher.sphereCollider.radius = pushRadius;
			pusher.transform.position = samplePoints[i];
			pusher.gameObject.SetActive(usePushers && !containerHits[i]);
		}
	}

	// returns the best fill set
	public FillSet ScoreAllFillSetsRaycast()
	{
		FindOverallBoundingBox();
		//FindOverallBoundingBoxWithFillSets();

		float bestScore = Mathf.NegativeInfinity;
		FillSet best = null;
		// fill the raycast command with rays coming from a ellipse around the bounding box towards the center
		Vector3 radius = overallBounds.size;
		Vector3 center = overallBounds.center;
		Bounds doubleBounds = overallBounds;
		doubleBounds.size = doubleBounds.size * 2.0f;
		float rayLength = radius.magnitude;

		NativeArray<RaycastCommand> raycastCommandArray = new NativeArray<RaycastCommand>(sampleCount * 2, Allocator.Temp);
		NativeArray<RaycastHit> raycastHits = new NativeArray<RaycastHit>(sampleCount * 2, Allocator.Temp);

		for (int i = 0; i < sampleCount; i++)
		{
			Ray ray = new Ray(Vector3.zero, Vector3.up);

			RaycastOriginShape shape = raycastOriginShape;

			if (shape == RaycastOriginShape.Random)
			{
				shape = (RaycastOriginShape) Random.Range(0, 2);
			}

			switch(shape)
			{
				case RaycastOriginShape.Box:
					ray = GetRandomBoxRay(doubleBounds);
					break;

				case RaycastOriginShape.Ellipsoid:
					ray = GetEllipsoidRay(center, radius);
					break;
			}

			Vector3 direction = ray.direction;
			Vector3 from = ray.origin;

			raycastCommandArray[i * 2] = new RaycastCommand(from, direction, rayLength, containerLayerMask, 1);
			raycastCommandArray[i * 2 + 1] = new RaycastCommand(from, direction, rayLength, fillLayerMask, 1);
		}

		float adjustedGoodRadius = raycastShrinkingGoodEnoughRadius ? raycastGoodEnoughRadius * mutationMultiplier : raycastGoodEnoughRadius;

		// loop through fillSets, and activate one by one
		// and do the raycasts
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			fillSet.gameObject.SetActive(true);

			// cast against the container and fills
			var raycastHandle = RaycastCommand.ScheduleBatch(raycastCommandArray, raycastHits, 1);
			raycastHandle.Complete();

			float score = 0.0f;


			// compare results and score
			for (int j = 0; j < sampleCount; j++)
			{
				RaycastHit containerHit = raycastHits[j * 2];
				Collider containerCollider = containerHit.collider;
				RaycastHit fillHit = raycastHits[j * 2 + 1];
				Collider fillCollider = fillHit.collider;

				if (containerCollider != null && fillCollider != null)
				{
					float containerDist = containerHit.distance;
					float fillDist = fillHit.distance;

					float dist = Mathf.Abs(containerDist - fillDist);

					float inverseLerp = Mathf.InverseLerp(adjustedGoodRadius, 0.0f, dist);
					score += inverseLerp;
				}
				else if (containerCollider == null && fillCollider == null)
				{
					score += 1.0f;
				}

			}

			fillSet.score = score / ((float)sampleCount);
			fillSet.gameObject.SetActive(false);
		}

		cumulativeScore = 0.0f;

		// calculate the cumulative score
		for (int i = 0; i < populationSize; i++)
		{
			FillSet fillSet = fillSets[i];
			cumulativeScore += fillSet.score;
			fillSet.spinner = cumulativeScore;
			if (fillSet.score > bestScore)
			{
				best = fillSet;
				bestScore = fillSet.score;
			}
		}

		raycastCommandArray.Dispose();
		raycastHits.Dispose();

		return best;
	}

	private bool ContainerBoundCheck(Vector3 pos)
	{
		bool result = false;
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].bounds.Contains(pos))
			{
				result = true;
				break;
			}
		}

		return result;
	}

	[ContextMenu("Find Overall Bounds")]
	private Bounds FindOverallBoundingBox()
	{
		Bounds b = colliders[0].bounds;

		for (int i = 0; i < colliders.Length; i++)
		{
			Bounds colliderBounds = colliders[i].bounds;
			b.Encapsulate(colliderBounds);
		}

		overallBounds = b;
		return b;
	}

	public Bounds FindOverallBoundingBoxWithFillSets()
	{
		FindOverallBoundingBox();

		for (int i = 0; i < fillSets.Length; i++)
		{
			Bounds b = fillSets[i].CalculateOverallBounds();
			overallBounds.Encapsulate(b);
		}
		return overallBounds;
	}

}
