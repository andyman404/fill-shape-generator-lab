﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBoundsLineDrawer : MonoBehaviour {

	public struct BoundBox
	{
		public Bounds bounds;
		public Color color;

		public BoundBox(Bounds bounds, Color color)
		{
			this.bounds = bounds;
			this.color = color;
		}
	}

	public List<BoundBox> boundsToDraw = new List<BoundBox>();
	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			Shader shader = Shader.Find("Hidden/Internal-Colored");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}

	public void AddBoundBlock(Bounds bounds, Color color)
	{
		boundsToDraw.Add(new BoundBox(bounds, color));
	}
	void OnPostRender()
	{
		CreateLineMaterial();
		lineMaterial.SetPass(0);

		GL.PushMatrix();
		GL.MultMatrix(Matrix4x4.identity);
		GL.Begin(GL.LINES);

		for(int i = 0; i < boundsToDraw.Count; i++)
		{

			BoundBox boundBox = boundsToDraw[i];
			GL.Color(boundBox.color);

			Vector3 min = boundBox.bounds.min;
			Vector3 max = boundBox.bounds.max;

			Vector3 a = min;
			Vector3 b = new Vector3(min.x, min.y, max.z);
			Vector3 c = new Vector3(max.x, min.y, max.z);
			Vector3 d = new Vector3(max.x, min.y, min.z);

			Vector3 e = new Vector3(min.x, max.y, min.z);
			Vector3 f = new Vector3(min.x, max.y, max.z);
			Vector3 g = max;
			Vector3 h = new Vector3(max.x, max.y, min.z);

			// bottom reect
			GL.Vertex(a);
			GL.Vertex(b);

			GL.Vertex(b);
			GL.Vertex(c);

			GL.Vertex(c);
			GL.Vertex(d);

			GL.Vertex(d);
			GL.Vertex(a);

			// top rect
			GL.Vertex(e);
			GL.Vertex(f);

			GL.Vertex(f);
			GL.Vertex(g);

			GL.Vertex(g);
			GL.Vertex(h);

			GL.Vertex(h);
			GL.Vertex(e);

			// vertical lines
			GL.Vertex(a);
			GL.Vertex(e);

			GL.Vertex(b);
			GL.Vertex(f);

			GL.Vertex(c);
			GL.Vertex(g);

			GL.Vertex(d);
			GL.Vertex(h);

		}

		GL.End();
		GL.PopMatrix();

		boundsToDraw.Clear();
	}

}
