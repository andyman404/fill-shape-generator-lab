﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatGroup : MonoBehaviour {

	public Text valueLabel;
	public Text descriptionLabel;

	public string value;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (valueLabel.text != value)
		{
			valueLabel.text = value;
		}
	}
}
