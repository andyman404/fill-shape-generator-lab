﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LabelButtonGroupController : MonoBehaviour {

	public string labelText;
	public string[] buttonText;

	public Text labelTextUI;
	public Text[] buttonTextUI;
	public Slider slider;

	public Sprite offSprite;
	public Sprite onSprite;

	public Color onTextColor;
	public Color offTextColor;

	// Use this for initialization
	void Start () {
		RefreshUI();
	}
	
	// Update is called once per frame
	void Update () {
		RefreshUI();
	}

	public void SelectButton(int buttonIndex)
	{
		for(int i = 0; i < buttonTextUI.Length; i++)
		{
			Image buttonSprite = buttonTextUI[i].transform.parent.GetComponent<Image>();
			Text buttonText = buttonTextUI[i];
			if (i == buttonIndex)
			{
				buttonSprite.sprite = onSprite;
				buttonText.color = onTextColor;
			}
			else
			{
				buttonSprite.sprite = offSprite;
				buttonText.color = offTextColor;
			}
		}
	}

	void RefreshUI()
	{

		if (labelTextUI != null)
		{

			if (labelTextUI.text != labelText)
			{
				labelTextUI.text = labelText;
			}
		}
		
		for(int i = 0; i < buttonText.Length; i++)
		{
			if (buttonTextUI[i] != null)
			{
				if (buttonTextUI[i].text != buttonText[i])
				{
					buttonTextUI[i].text = buttonText[i];
				}
			}
		}
	}
}
