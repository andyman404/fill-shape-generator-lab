﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Text;

public class DemoController : MonoBehaviour {

	public GameObject[] targets;
	public GameObject[] algorithms;
	public GameObject[] fillTypesGenetic;
	public GameObject[] fillTypesRB;

	public int targetShapeIndex;
	public int algorithmIndex;
	public int geneticFillTypeIndex;
	public int rbFillTypeIndex;
	public int populationSizeIndex;
	public int[] populationSizeValues = new int[] { 10, 50, 100, 250, 500, 1000 };
	public int fillersCount = 50;
	public int sampleCount = 1024;
	public float minMutationRate = 0.01f;

	public LabelButtonGroupController fillTargetButtonGroup;
	public LabelButtonGroupController algorithmButtonGroup;
	public LabelButtonGroupController fillTypeGeneticButtonGroup;
	public LabelButtonGroupController fillTypeRBButtonGroup;
	public LabelButtonGroupController populationSizeButtonGroup;
	public LabelButtonGroupController fillersCountButtonGroup;
	public LabelButtonGroupController sampleCountButtonGroup;
	public LabelButtonGroupController minMutationRateButtonGroup;

	public GameObject configUI;
	public GameObject progressUI;

	public Material containerMaterial;
	public Material fillMaterialOpaque;
	public Material fillMaterialTransparent;
	public Material fillMaterial;

	public float fillMaterialOpacity;
	public GameObject fillOpaqueButton;
	public GameObject fillTransparentButton;
	public GameObject lessMutationButton, moreMutationButton;
	public bool targetsVisible = true;
	public GameObject targetsVisibleButton, targetsHiddenButton;

	public void RefreshTargetVisibility()
	{
		MeshRenderer[] renderers = targets[targetShapeIndex].GetComponentsInChildren<MeshRenderer>(true);
		for(int i = 0; i < renderers.Length; i++)
		{
			renderers[i].enabled = targetsVisible;
		}

		targetsVisibleButton.SetActive(targetsVisible);
		targetsHiddenButton.SetActive(!targetsVisible);
	}

	public void ShowTargets()
	{
		targetsVisible = true;
		RefreshTargetVisibility();
	}

	public void HideTargets()
	{
		targetsVisible = false;
		RefreshTargetVisibility();
	}
	public void RefreshFillMaterialOpacity()
	{
		fillMaterial = fillMaterialOpacity >= 0.95f ? fillMaterialOpaque : fillMaterialTransparent;
		Material oldMaterial = fillMaterialOpacity >= 0.95f ? fillMaterialTransparent: fillMaterialOpaque;

		bool isOpaque = (fillMaterial == fillMaterialOpaque);

		fillOpaqueButton.SetActive(isOpaque);
		fillTransparentButton.SetActive(!isOpaque);

		if (fillGenerator != null)
		{
			MeshRenderer[] renderers = fillGenerator.GetComponentsInChildren<MeshRenderer>(true);
			for(int i = 0; i < renderers.Length; i++)
			{
				if (renderers[i].sharedMaterial == oldMaterial)
				{
					renderers[i].sharedMaterial = fillMaterial;
				}
			}
		}
		else if (recursiveFiller != null)
		{
			recursiveFiller.fillMaterial = fillMaterial;
			MeshRenderer[] renderers = recursiveFiller.GetComponentsInChildren<MeshRenderer>(true);
			for (int i = 0; i < renderers.Length; i++)
			{
				if (renderers[i].sharedMaterial == oldMaterial)
				{
					renderers[i].sharedMaterial = fillMaterial;
				}
			}
		}
	}	

	[ContextMenu("Transparent")]
	public void SetFillTransparent()
	{
		fillMaterialOpacity = 0.5f;
		RefreshFillMaterialOpacity();
	}

	[ContextMenu("Opaque")]
	public void SetFillOpaque()
	{
		fillMaterialOpacity = 1.0f;
		RefreshFillMaterialOpacity();
	}

	public void RefreshStats()
	{
		if (fillGenerator != null)
		{
			RefreshStatsFillGenerator();
		}
		else if (recursiveFiller != null)
		{
			RefreshStatsRecursiveFiller();
		}
	}

	public StatGroup bestFitnessStat;
	public StatGroup timeElapsedStat;
	public StatGroup generationsStat;
	public StatGroup depthStat;
	public StatGroup progressStat;
	public StatGroup fillPiecesStat;
	public StatGroup mutationMultiplierStat;
	public Text experimentDescription;
	public void RefreshStatsFillGenerator()
	{
		RefreshBestFitnessStatForFillGenerator();
		RefreshTimeElapsedStatForFillGenerator();
		RefreshGenerationsStatForFillGenerator();
		mutationMultiplierStat.value = string.Format("{0:0.000}",fillGenerator.mutationMultiplier);
	}

	private void RefreshGenerationsStatForFillGenerator()
	{
		generationsStat.value = fillGenerator.generations.ToString();
	}

	private void RefreshBestFitnessStatForFillGenerator()
	{
		bestFitnessStat.value = string.Format("{0:0.00}%", fillGenerator.bestFillSetScore * 100.0f);
	}

	private void RefreshTimeElapsedStatForFillGenerator()
	{
		float timeElapsed = timeElapsedBeforeStart;
		if (fillGenerator.enabled)
		{
			timeElapsed += Time.time - startTime;
		}

		timeElapsed = DisplayTimeElapsed(timeElapsed);
	}

	private void RefreshTimeElapsedStatForRecursiveFiller()
	{
		float timeElapsed = timeElapsedBeforeStart;
		if (recursiveFiller.running && !recursiveFiller.paused)
		{
			timeElapsed += Time.time - startTime;
		}

		timeElapsed = DisplayTimeElapsed(timeElapsed);
	}

	private float DisplayTimeElapsed(float timeElapsed)
	{
		int hours = Mathf.FloorToInt(timeElapsed / 3600.0f);
		timeElapsed -= ((float)hours) * 3600.0f;

		int minutes = Mathf.FloorToInt(timeElapsed / 60.0f);
		timeElapsed -= ((float)minutes) * 60.0f;

		float seconds = timeElapsed;

		timeElapsedStat.value = string.Format("{0}:{1:00}:{2:00}", hours, minutes, seconds);
		return timeElapsed;
	}

	public void RefreshStatsRecursiveFiller()
	{
		bestFitnessStat.value = string.Format ("{0:0.00}%", recursiveFiller.fitness*100.0f);
		RefreshTimeElapsedStatForRecursiveFiller();

		depthStat.value = string.Format("{0}/{1}", recursiveFiller.depthReached, recursiveFiller.maxDepth);
		progressStat.value = string.Format("{0:0}%", recursiveFiller.progressInDepth*100.0f);
	}
	// Use this for initialization
	void Start()
	{
		progressUI.SetActive(false);
		configUI.SetActive(true);
		RefreshConfigUI();
		ShowTargets();
	}

	float nextUpdateTime = 0;

	// Update is called once per frame
	void Update()
	{
		if (progressUI.activeSelf && Time.time > nextUpdateTime)
		{
			nextUpdateTime = Time.time + 0.25f;
			RefreshStats();
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	public void RefreshConfigUI()
	{
		RefreshTargetGroup();
		RefreshAlgorithmGroup();
		RefreshGeneticFillTypeGroup();
		RefreshRBFillTypeGroup();
		RefreshPopulationSizeSlider();
		RefreshFillersCountSlider();
		RefreshSampleCountSlider();

		RefreshFillThresholdLeafSlider();
		RefreshFillThresholdSlider();
		RefreshMinSideLengthSlider();
		RefreshRecursionDepthSlider();
		RefreshMinMutationRateMultiplierSlider();
	}

	public void NextTarget()
	{
		targetShapeIndex = WrapValue(targetShapeIndex+1, targets.Length);
		RefreshConfigUI();
	}
	public void PrevTarget()
	{
		targetShapeIndex = WrapValue(targetShapeIndex - 1, targets.Length);
		RefreshConfigUI();
	}
	private int WrapValue(int value, int length)
	{
		int result = value;
		if (result >= length)
		{
			result = 0;
		}
		else if (result < 0)
		{
			result = length - 1;
		}

		return result;
	}
	public void RefreshTargetGroup()
	{
		// hide all the targets
		for(int i = 0; i < targets.Length; i++)
		{
			targets[i].SetActive(false);
		}

		// update the target
		targets[targetShapeIndex].SetActive(true);

		// update the label
		fillTargetButtonGroup.labelText = "Target Shape: " + targets[targetShapeIndex].name;

	}

	public void NextAlgorithm()
	{
		algorithmIndex = WrapValue(algorithmIndex + 1, algorithms.Length);
		RefreshConfigUI();
	}
	public void PrevAlgorithm()
	{
		algorithmIndex = WrapValue(algorithmIndex - 1, algorithms.Length);
		RefreshConfigUI();
	}
	public void RefreshAlgorithmGroup()
	{
		// update the label
		algorithmButtonGroup.labelText = "Algorithm: " + algorithms[algorithmIndex].name;
	}

	public void NextGeneticFillType()
	{
		geneticFillTypeIndex = WrapValue(geneticFillTypeIndex + 1, fillTypesGenetic.Length);

		RefreshConfigUI();
	}
	public void PrevGeneticFillType()
	{
		geneticFillTypeIndex = WrapValue(geneticFillTypeIndex - 1, fillTypesGenetic.Length);
		RefreshConfigUI();

	}
	public void RefreshGeneticFillTypeGroup()
	{
		// update the label
		fillTypeGeneticButtonGroup.labelText = "Filler: " + fillTypesGenetic[geneticFillTypeIndex].name;
		fillTypeGeneticButtonGroup.gameObject.SetActive(algorithmIndex == 0 || algorithmIndex == 1);
	}

	public void NextRBFillType()
	{
		rbFillTypeIndex = WrapValue(rbFillTypeIndex + 1, fillTypesRB.Length);
		RefreshConfigUI();
	}
	public void PrevRBFillType()
	{
		rbFillTypeIndex = WrapValue(rbFillTypeIndex - 1, fillTypesRB.Length);
		RefreshConfigUI();

	}
	public void RefreshRBFillTypeGroup()
	{
		// update the label
		fillTypeRBButtonGroup.labelText = "Filler: " + fillTypesRB[rbFillTypeIndex].name;
		fillTypeRBButtonGroup.gameObject.SetActive(algorithmIndex == 2);
	}

	public void PopulationSizeSliderChanged()
	{
		populationSizeIndex = (int)populationSizeButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshPopulationSizeSlider()
	{
		populationSizeButtonGroup.labelText = "Population Size: " + populationSizeValues[populationSizeIndex] + (populationSizeIndex > 1 ? " (Slow!!!)" : "");
		populationSizeButtonGroup.gameObject.SetActive(algorithmIndex < 3);
		populationSizeButtonGroup.slider.value = populationSizeIndex;
	}

	public void FillersCountSliderChanged()
	{
		fillersCount = (int)fillersCountButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshFillersCountSlider()
	{
		fillersCountButtonGroup.labelText = "Fillers Per Individual: " + fillersCount;
		fillersCountButtonGroup.gameObject.SetActive(algorithmIndex < 3);
		fillersCountButtonGroup.slider.value = fillersCount;
	}

	public void SampleCountSliderChanged()
	{
		sampleCount = (int)sampleCountButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshSampleCountSlider()
	{
		sampleCountButtonGroup.labelText = "Sampling Count: " + sampleCount;
		sampleCountButtonGroup.slider.value = sampleCount;
	}

	public void MinMutationRateMultiplierSliderChanged()
	{
		minMutationRate = minMutationRateButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshMinMutationRateMultiplierSlider()
	{
		minMutationRateButtonGroup.labelText = "Minimum Mutation Rate Multiplier: " + minMutationRate;
		minMutationRateButtonGroup.slider.value = minMutationRate;
		minMutationRateButtonGroup.gameObject.SetActive(algorithmIndex < 3);
	}

	public LabelButtonGroupController recursionDepthButtonGroup;
	public LabelButtonGroupController minSideLengthButtonGroup;
	public LabelButtonGroupController fillThresholdButtonGroup;
	public LabelButtonGroupController fillThresholdLeafButtonGroup;
	public int recursionDepth = 8;
	public float minSideLength = 0.1f;
	public float fillThreshold = 0.85f;
	public float fillThresholdLeaf = 0.1f;
	public float startTime;
	public float timeElapsedBeforeStart;

	public void FillThresholdLeafSliderChanged()
	{
		fillThresholdLeaf = fillThresholdLeafButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshFillThresholdLeafSlider()
	{
		fillThresholdLeafButtonGroup.labelText = "Fill Threshold for Final Depth: " + FormatDecimal(fillThresholdLeaf);
		fillThresholdLeafButtonGroup.slider.value = fillThresholdLeaf;
		fillThresholdLeafButtonGroup.gameObject.SetActive(algorithmIndex == 3);
	}

	public void FillThresholdSliderChanged()
	{
		fillThreshold = fillThresholdButtonGroup.slider.value;
		RefreshConfigUI();
	}
	public void RefreshFillThresholdSlider()
	{
		fillThresholdButtonGroup.labelText = "Fill Threshold: " + FormatDecimal(fillThreshold);
		fillThresholdButtonGroup.slider.value = fillThreshold;
		fillThresholdButtonGroup.gameObject.SetActive(algorithmIndex == 3);
	}
	private string FormatDecimal(float value)
	{
		return string.Format("{0:0.00}", value);
	}

	public void MinSideLengthSliderChanged()
	{
		minSideLength = minSideLengthButtonGroup.slider.value;
		RefreshConfigUI();
	}
	public void RefreshMinSideLengthSlider()
	{
		minSideLengthButtonGroup.labelText = "Minimum Side Length of Filler Block: " + FormatDecimal(minSideLength);
		minSideLengthButtonGroup.slider.value = minSideLength;
		minSideLengthButtonGroup.gameObject.SetActive(algorithmIndex == 3);

	}

	public void RecursionDepthSliderChanged()
	{
		recursionDepth = (int) recursionDepthButtonGroup.slider.value;
		RefreshConfigUI();
	}

	public void RefreshRecursionDepthSlider()
	{
		recursionDepthButtonGroup.labelText = "Maximum Recursion Depth: " + recursionDepth;
		recursionDepthButtonGroup.slider.value = recursionDepth;
		recursionDepthButtonGroup.gameObject.SetActive(algorithmIndex == 3);

	}

	private FillGenerator fillGenerator = null;
	private RecursiveMeshFiller recursiveFiller = null;
	public GameObject pushersOnButton, pushersOffButton, fixStraysButton, pauseButton, resumeButton;

	public void Run()
	{
		timeElapsedBeforeStart = 0.0f;
		StringBuilder buf = new StringBuilder();
		buf.AppendLine(algorithms[algorithmIndex].name);

		if (algorithmIndex <= 2)
		{
			fillGenerator = Instantiate<FillGenerator>(algorithms[algorithmIndex].GetComponent<FillGenerator>());

			fillGenerator.targetContainer = targets[targetShapeIndex].transform;
			buf.AppendLine("Target: " + targets[targetShapeIndex].name);

			fillGenerator.populationSize = populationSizeValues[populationSizeIndex];
			buf.AppendLine("Population Size: " + fillGenerator.populationSize);

			fillGenerator.fillsPerSet = fillersCount;
			buf.AppendLine("Fills Per Individual: " + fillersCount);
			if (algorithmIndex <= 1)
			{
				fillGenerator.fillPrefabs[0] = fillTypesGenetic[geneticFillTypeIndex].GetComponent<Fill>();
			}
			else
			{
				fillGenerator.fillPrefabs[0] = fillTypesRB[rbFillTypeIndex].GetComponent<Fill>();
			}
			buf.AppendLine("Fill Type: " + fillGenerator.fillPrefabs[0].name);

			fillGenerator.sampleCount = sampleCount;
			buf.AppendLine("Sample Count: " + sampleCount);

			fillGenerator.minMutationMultiplier = minMutationRate;
			buf.AppendLine("Min Mutation Rate: " + minMutationRate);


			fillGenerator.gameObject.SetActive(true);
			startTime = Time.time;
			bestFitnessStat.gameObject.SetActive(true);
			timeElapsedStat.gameObject.SetActive(true);
			generationsStat.gameObject.SetActive(true);
			depthStat.gameObject.SetActive(false);
			progressStat.gameObject.SetActive(false);
			fillPiecesStat.gameObject.SetActive(false);
			mutationMultiplierStat.gameObject.SetActive(true);
			pushersOnButton.SetActive(fillGenerator.volumetricUsePushers && algorithmIndex == 2);
			pushersOffButton.SetActive(!fillGenerator.volumetricUsePushers && algorithmIndex == 2);
			fixStraysButton.SetActive(true);
			lessMutationButton.SetActive(true);
			moreMutationButton.SetActive(true);
			RefreshFillMaterialOpacity();
		}
		else
		{
			recursiveFiller = Instantiate<RecursiveMeshFiller>(algorithms[algorithmIndex].GetComponent<RecursiveMeshFiller>());
			recursiveFiller.runOnStart = true;
			recursiveFiller.doneEvent += RecursiveFillerDone;
			recursiveFiller.targetContainer = targets[targetShapeIndex].transform;
			buf.AppendLine("Target: " + targets[targetShapeIndex].name);

			recursiveFiller.checksPerBlock = sampleCount;
			buf.AppendLine("Sample Count: " + sampleCount);

			recursiveFiller.maxDepth = recursionDepth;
			buf.AppendLine("Max Recursion Depth: " + recursionDepth);

			recursiveFiller.minSide = minSideLength;
			buf.AppendLine("Minimum Side Length of Filler Block: " + minSideLength);

			recursiveFiller.minChecksHitRatio = fillThreshold;
			buf.AppendLine("Fill Threshold: " + fillThreshold);

			recursiveFiller.leafHitRatio = fillThresholdLeaf;
			buf.AppendLine("Fill Threshold for Final Depth: " + fillThresholdLeaf);

			recursiveFiller.gameObject.SetActive(true);
			recursiveFiller.boundsLineDrawer = Camera.main.GetComponent<CameraBoundsLineDrawer>();
			startTime = Time.time;
			bestFitnessStat.gameObject.SetActive(true);
			timeElapsedStat.gameObject.SetActive(true);
			generationsStat.gameObject.SetActive(false);
			depthStat.gameObject.SetActive(true);
			progressStat.gameObject.SetActive(true);
			fillPiecesStat.gameObject.SetActive(false);
			mutationMultiplierStat.gameObject.SetActive(false);
			pushersOnButton.SetActive(false);
			pushersOffButton.SetActive(false);
			fixStraysButton.SetActive(false);
			lessMutationButton.SetActive(false);
			moreMutationButton.SetActive(false);
			fillOpaqueButton.SetActive(false);
			fillTransparentButton.SetActive(false);


		}
		experimentDescription.text = buf.ToString();
		experimentDescription.gameObject.SetActive(true);
		configUI.SetActive(false);
		progressUI.SetActive(true);
		RefreshTargetVisibility();
	}


	public void RecursiveFillerDone()
	{
		timeElapsedBeforeStart += Time.time + startTime;
		depthStat.gameObject.SetActive(false);
		progressStat.gameObject.SetActive(false);
		fillPiecesStat.value = recursiveFiller.fillPieces.ToString();
		fillPiecesStat.gameObject.SetActive(true);
		RefreshFillMaterialOpacity();
	}

	public void Pause()
	{
		Time.timeScale = 0.0f;
		if (fillGenerator != null)
		{
			timeElapsedBeforeStart += Time.time - startTime;
			fillGenerator.enabled = false;
		}
		else if (recursiveFiller != null)
		{
			if (recursiveFiller.running)
			{
				timeElapsedBeforeStart += Time.time - startTime;
			}
			recursiveFiller.Pause();
		}
		pauseButton.gameObject.SetActive(false);
		resumeButton.gameObject.SetActive(true);
	}

	public void Resume()
	{
		Time.timeScale = 1.0f;
		startTime = Time.time;
		if (fillGenerator != null)
		{
			fillGenerator.enabled = true;
		}
		else if (recursiveFiller != null)
		{
			recursiveFiller.Resume();
		}
		pauseButton.gameObject.SetActive(true);
		resumeButton.gameObject.SetActive(false);
	}

	public void FixStrays()
	{
		if (fillGenerator != null)
		{
			fillGenerator.bounceTriggered = true;
		}
	}

	public void TurnPushersOff()
	{
		if (fillGenerator != null)
		{
			fillGenerator.volumetricUsePushers = false;
		}
	}

	public void TurnPushersOn()
	{
		if (fillGenerator != null)
		{
			fillGenerator.volumetricUsePushers = true;
		}
	}
	public void Abort()
	{
		experimentDescription.gameObject.SetActive(false);

		Resume();
		if (fillGenerator != null)
		{
			Destroy(fillGenerator.gameObject);
		}
		else if (recursiveFiller != null)
		{
			Destroy(recursiveFiller.gameObject);
		}
		fillGenerator = null;
		recursiveFiller = null;
		progressUI.SetActive(false);
		configUI.SetActive(true);
		ShowTargets();
	}

	public void MoreMutation()
	{
		if (fillGenerator != null)
		{
			fillGenerator.TriggerMutationIncrement();
			RefreshStats();
		}
	}

	public void LessMutation()
	{
		if (fillGenerator != null)
		{
			fillGenerator.TriggerMutationDecrement();
			RefreshStats();
		}
	}
	public void Quit()
	{
		Application.Quit();
	}


}
