﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRotator : MonoBehaviour {
	public Vector3 rotationRate;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.localRotation *= Quaternion.Euler(rotationRate * Time.deltaTime);	
	}
}
