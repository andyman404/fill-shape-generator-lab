﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillTester : MonoBehaviour {

	public LayerMask containerLayer;
	public BoxCollider box;
	public Bounds boxBounds;
	public int sampleSize = 1024;
	public float filled = 0.0f;
	public bool centerIsInside = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		boxBounds = box.bounds;
		filled = PointCheckUtil.CheckFills(boxBounds, containerLayer, sampleSize, 100.0f);
		centerIsInside = PointCheckUtil.CheckIfPointInside(transform.position, containerLayer, 100.0f);
	}
}
