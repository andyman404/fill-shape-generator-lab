﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class RecursiveMeshFiller : MonoBehaviour
{
	public delegate void DoneDelegate();
	public event DoneDelegate doneEvent;

	public Transform targetContainer;

	private Collider[] colliders;
	private int[] originalLayers;

	public int containerLayer;
	public int fillLayer;

	public LayerMask containerLayerMask;
	public LayerMask fillLayerMask;

	public int splits = 2;
	public int maxDepth = 4;
	public float minSide = 0.2f;

	[HideInInspector]
	private GameObject targetFill;
	public BoxCollider fillBlockPrefab;
	private Queue<Bounds> processQueue = new Queue<Bounds>();

	public int checksPerBlock = 1024;
	public float minChecksHitRatio = 0.85f;
	public float leafHitRatio = 0.5f;
	public Vector3[] directions = new Vector3[]
	{
		Vector3.up,
		Vector3.forward,
		Vector3.down,
		Vector3.back,
		Vector3.right,
		Vector3.left
	};
	private Bounds containerBounds;
	private int fillsGenerated = 0;
	private float progress = 0.0f;
	private bool canceled = false;
	[HideInInspector]
	public List<BoxCollider> fillBlocks = new List<BoxCollider>();
	private List<CombinationCheck> combinationChecks = new List<CombinationCheck>();

	public bool runOnStart = false;
	public bool showBoundsLines = true;
	public CameraBoundsLineDrawer boundsLineDrawer = null;

	public bool running = false;
	public Coroutine runningCoroutine = null;

	public Material fillMaterial;

	// Use this for initialization
	void Start()
	{
		if (runOnStart)
		{
			runningCoroutine = StartCoroutine(Run());
		}
	}
	public void Pause()
	{
		paused = true;
	}

	public void Resume()
	{
		paused = false;
	}

	// Update is called once per frame
	void Update()
	{
		if (showBoundsLines && Application.isPlaying)
		{
			DrawBoundsLines();
		}
	}

	private void DrawBoundsLines()
	{
		if (boundsLineDrawer != null)
		{
			// draw the line bounds
			for (int i = 0; i < fillBlocks.Count; i++)
			{
				if (fillBlocks[i].gameObject.activeSelf)
				{
					Bounds bounds = fillBlocks[i].bounds;
					//bounds.extents *= 0.95f;

					boundsLineDrawer.AddBoundBlock(fillBlocks[i].bounds, Color.green);
				}
			}

			// draw the queued stuff
			Bounds[] queuedBounds = processQueue.ToArray();
			Color inProgressColor = Color.red;
			inProgressColor.a = 0.5f;

			for (int i = 0; i < queuedBounds.Length; i++)
			{
				Bounds bounds = queuedBounds[i];
				bounds.extents *= 0.95f;

				boundsLineDrawer.AddBoundBlock(bounds, inProgressColor);
			}
		}
	}

	[ContextMenu("Run All")]
	public void RunAll()
	{
		running = true;
		RecursiveFill();
		Combine();
		RecenterBlocksAndMakeVisible();
		running = false;
		fillPieces = fillBlocks.Count;
		doneEvent();
	}

	public IEnumerator Run()
	{
		running = true;
		yield return StartCoroutine(RecursiveFillCoroutine());
		Combine();
		RecenterBlocksAndMakeVisible();
		running = false;
		fillPieces = fillBlocks.Count;
		doneEvent();
	}


	[ContextMenu("Clear")]
	public void Clear()
	{
		// delete any previous fill wrapper
		Transform oldFillWrapper = transform.Find("FillWrapper");
		if (oldFillWrapper != null)
		{
			if (Application.isPlaying)
			{
				Destroy(oldFillWrapper.gameObject);
			}
			else
			{
				DestroyImmediate(oldFillWrapper.gameObject);
			}
		}
	}

	private void AddFillWrapper()
	{
		Clear();

		targetFill = new GameObject("FillWrapper");
		targetFill.transform.rotation = Quaternion.identity;
		targetFill.transform.position = transform.position;
		targetFill.transform.parent = transform;

	}

	private void InitializeContainers()
	{
		colliders = targetContainer.GetComponentsInChildren<Collider>();
		originalLayers = new int[colliders.Length];
		for (int i = 0; i < colliders.Length; i++)
		{
			GameObject g = colliders[i].gameObject;
			originalLayers[i] = g.layer;
			g.layer = containerLayer;
		}
	}

	private Bounds CalculateContainerBounds()
	{
		Bounds b = colliders[0].bounds;
		for (int i = 1; i < colliders.Length; i++)
		{
			b.Encapsulate(colliders[i].bounds);
		}
		return b;
	}



	[ContextMenu("Recursive Fill")]
	public void RecursiveFill()
	{
		if (fillBlocks != null)
		{
			fillBlocks.Clear();
		}
		else
		{
			fillBlocks = new List<BoxCollider>();
		}

		AddFillWrapper();
		InitializeContainers();

		containerBounds = CalculateContainerBounds();
		canceled = false;

		fillsGenerated = 0;
		progress = 0;
		processQueue.Clear();

		processQueue.Enqueue(containerBounds);
		for (int currentDepth = 0; currentDepth <= maxDepth; currentDepth++)
		{
			depthReached = currentDepth;

			string depthString = "Processing Depth " + currentDepth + "/" + maxDepth;
			int queueSize = processQueue.Count;
			for (int i = 0; i < queueSize; i++)
			{
				progressInDepth = ((float)(i+1)) / ((float)queueSize);

				Bounds bounds = processQueue.Dequeue();
				RecursiveSplitAndFill(bounds, currentDepth);
#if UNITY_EDITOR
				if (!Application.isPlaying && EditorUtility.DisplayCancelableProgressBar("Recursive Split and Fill", depthString, ((float)i) / ((float)queueSize)))
				{
					canceled = true;
				}
#endif
			}
			if (canceled)
				break;
		}
		fillPieces = fillsGenerated;

		Debug.Log("Fills: " + fillsGenerated);

#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			EditorUtility.ClearProgressBar();
		}
#endif
		CheckFitness();
	}
	public bool paused = false;
	public int depthReached = 0;
	public float progressInDepth = 0.0f;
	public int fillPieces = 0;

	public IEnumerator RecursiveFillCoroutine()
	{
		if (fillBlocks != null)
		{
			fillBlocks.Clear();
		}
		else
		{
			fillBlocks = new List<BoxCollider>();
		}

		AddFillWrapper();
		InitializeContainers();

		containerBounds = CalculateContainerBounds();
		canceled = false;

		fillsGenerated = 0;
		progress = 0;
		processQueue.Clear();

		processQueue.Enqueue(containerBounds);
		int steps = 0;
		for (int currentDepth = 0; currentDepth <= maxDepth; currentDepth++)
		{
			depthReached = currentDepth;
			yield return 0;
			int queueSize = processQueue.Count;
			for (int i = 0; i < queueSize; i++)
			{
				Bounds bounds = processQueue.Dequeue();
				RecursiveSplitAndFill(bounds, currentDepth);
				progressInDepth = ((float)(i+1)) / ((float)queueSize);
				if (steps % 8 == 0)
				{
					yield return 0;
				}
				if (paused)
				{
					CheckFitness();
				}
				while (paused)
				{
					yield return new WaitForSeconds(0.1f);
				}
			}
			CheckFitness();

			if (canceled)
				break;

		}
		fillPieces = fillsGenerated;
	}
	private Bounds CalculateContainerAndFillBounds()
	{
		Collider[] allColliders = targetContainer.GetComponentsInChildren<Collider>();
		Bounds b = allColliders[0].bounds;
		for (int i = 1; i < allColliders.Length; i++)
		{
			b.Encapsulate(allColliders[i].bounds);
		}
		return b;
	}

	public float fitness = 0.0f;

	[ContextMenu("Check Fitness")]
	public void CheckFitness()
	{
		Bounds marginBounds = CalculateContainerAndFillBounds();
		fitness = PointCheckUtil.CompareFill(marginBounds, containerLayerMask, fillLayerMask, checksPerBlock * 4, containerBounds.size.magnitude);
		Debug.Log("Fitness: " + fitness);
	}

	// returns true if it was split, false if it was filled
	private void RecursiveSplitAndFill(Bounds bounds, int depth)
	{
		if (canceled) return;

		float totalFill = CheckBox(bounds, containerLayerMask);
		float side = Mathf.Min(Mathf.Min(bounds.size.x, bounds.size.y));

		if (totalFill <= 0.0f)
		{
			//Debug.Log("Empty");
		}
		else if (totalFill > minChecksHitRatio)
		{
			CreateFillBlock(bounds);
			//Debug.Log("Filled");
		}
		else if (depth < maxDepth && side >= minSide)
		{
			//Debug.Log("Splitting");
			// split into n regions along the biggest dimension of the bounds
			Bounds[] regions = SplitIntoRegions(bounds, splits);

			for (int i = 0; i < splits; i++)
			{
				if (canceled) return;
				processQueue.Enqueue(regions[i]);
			}
		}
		else if (totalFill > leafHitRatio)
		{
			CreateFillBlock(bounds);
		}
		else
		{
		}
	}

	public void CreateFillBlock(Bounds bounds)
	{
		BoxCollider fill = Instantiate<BoxCollider>(fillBlockPrefab, bounds.center, Quaternion.identity);
		//fill.transform.localScale = bounds.size;
		fill.size = bounds.size;

		fill.transform.parent = targetFill.transform;
		fillsGenerated++;
		fillBlocks.Add(fill);

		Renderer renderer = fill.GetComponent<Renderer>();
		if (renderer != null)
		{
			renderer.material = fillMaterial;
		}
	}

	private Bounds[] SplitIntoRegions(Bounds bounds, int splits)
	{
		Bounds[] result = new Bounds[splits];

		Vector3 segmentExtents = bounds.extents;
		Vector3 segmentOffset = Vector3.zero;
		float floatSplits = (float)splits;
		// split x
		if (bounds.extents.x >= bounds.extents.y && bounds.extents.x >= bounds.extents.z)
		{
			segmentExtents.x /= floatSplits;
			segmentOffset.x = segmentExtents.x * 2.0f;
		}
		// split y
		else if (bounds.extents.y >= bounds.extents.x && bounds.extents.y >= bounds.extents.z)
		{
			segmentExtents.y /= floatSplits;
			segmentOffset.y = segmentExtents.y * 2.0f;
		}
		// split z
		else if (bounds.extents.z >= bounds.extents.x && bounds.extents.z >= bounds.extents.y)
		{
			segmentExtents.z /= floatSplits;
			segmentOffset.z = segmentExtents.z * 2.0f;
		}

		Vector3 min = bounds.min;
		Vector3 segmentSize = segmentExtents * 2.0f;
		for (int i = 0; i < splits; i++)
		{
			Vector3 center = min + segmentExtents + (segmentOffset * ((float)i));
			result[i] = new Bounds(center, segmentSize);
		}
		return result;
	}

	private bool CheckBoxSimple(Bounds bounds, LayerMask layerMask)
	{
		return Physics.CheckBox(bounds.center, bounds.extents, Quaternion.identity, layerMask);
	}

	private float CheckBox(Bounds bounds, LayerMask layerMask)
	{
		return PointCheckUtil.CheckFills(bounds, layerMask, checksPerBlock, containerBounds.size.magnitude);

		//int hits = 0;
		//float radius = bounds.size.magnitude;

		//for (int i = 0; i < checksPerBlock; i++)
		//{
		//	if (canceled) return 0;

		//	Vector3 point = RandomPointInBounds(bounds);
		//	if (PointCheckUtil.CheckIfPointInside(point, layerMask, containerBounds))
		//	{
		//		if (showDebug)
		//		{
		//			debugHits.Add(point);
		//		}
		//		hits++;
		//	}
		//	else if (showDebug)
		//	{
		//		debugMisses.Add(point);
		//	}
		//}

		//float result = ((float)hits) / ((float)checksPerBlock);
		////Debug.Log(result);
		//return result;
	}

	private Vector3 RandomPointInBounds(Bounds b)
	{
		Vector3 min = b.min;
		Vector3 max = b.max;

		return new Vector3(
			Random.Range(min.x, max.x),
			Random.Range(min.y, max.y),
			Random.Range(min.z, max.z)
			);
	}



	[ContextMenu("Combine")]
	public void Combine()
	{
		// for each pass, loop through and combine
		int fillBlocksCount = fillBlocks.Count;
		int combinations = 0;

		float passProgress = 0.0f;
		int pass = 0;
		float progressIncrement = 1.0f / ((float)directions.Length) / ((float)fillBlocksCount);

		do
		{
			string passString = "Pass " + pass;
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				EditorUtility.DisplayProgressBar("Progress", passString, progress);
			}
#endif
			combinations = 0;

			// check in directions
			for (int d = 0; d < directions.Length; d++)
			{
				Vector3 direction = directions[d];

				for (int i = fillBlocksCount - 1; i >= 0; i--)
				{
					BoxCollider fillBlock = fillBlocks[i];
					combinations += RunBlockCombineForDirection(fillBlock, direction);
					passProgress += progressIncrement;
				}
			}



			//for (int i = fillBlocksCount - 1; i >= 0; i--)
			//{
			//	BoxCollider fillBlock = fillBlocks[i];

			//	for (int d = 0; d < directions.Length; d++)
			//	{
			//		Vector3 direction = directions[d];
			//		combinations += RunBlockCombineForDirection(fillBlock, direction);
			//		passProgress += progressIncrement;
			//	}
			//}
			Debug.Log("Reductions: " + combinations);
			fillPieces = fillBlocks.Count;
		} while (combinations > 0);
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			EditorUtility.ClearProgressBar();
		}
#endif

		PruneUnneededBlocks();

		//RecenterBlocksAndMakeVisible();
		Debug.Log("Blocks Left: " + fillBlocks.Count);
		fillPieces = fillBlocks.Count;
	}

	private int RunBlockCombineForDirection(BoxCollider fillBlock, Vector3 direction)
	{
		int combinations = 0;
		if (fillBlock == null) return combinations;

		if (fillBlock.gameObject.activeSelf)
		{
			BoxCollider otherBlock = CheckCombinable(fillBlock, direction);
			if (otherBlock != null)
			{
				// combine with other block, deactivate other block
				Bounds myBounds = fillBlock.bounds;
				Bounds otherBounds = otherBlock.bounds;

				myBounds.Encapsulate(otherBounds);
				fillBlock.transform.position = myBounds.center;
				fillBlock.center = Vector3.zero;
				fillBlock.size = myBounds.size;
				//fillBlock.center = fillBlock.transform.InverseTransformPoint(myBounds.center);
				//fillBlock.size = fillBlock.transform.InverseTransformVector(myBounds.size);

				otherBlock.enabled = false;
				otherBlock.gameObject.SetActive(false);

				combinations++;
				//Debug.Log("hit");
			}
			else
			{
				//Debug.Log("niss");
			}
		}
		return combinations;
	}

	[ContextMenu("Recenter and Make Visible")]
	public void RecenterBlocksAndMakeVisible()
	{
		// recenter blocks

		for (int i = 0; i < fillBlocks.Count; i++)
		{
			BoxCollider block = fillBlocks[i];
			Bounds bounds = block.bounds;
			block.transform.position = bounds.center;
			block.transform.localScale = block.transform.InverseTransformVector(bounds.size);
			block.center = Vector3.zero;
			block.size = Vector3.one;
			block.GetComponent<MeshRenderer>().enabled = true;
		}
	}

	private void PruneUnneededBlocks()
	{
		// prune away unneeded blocks
		List<BoxCollider> neededBlocks = new List<BoxCollider>();
		for (int i = 0; i < fillBlocks.Count; i++)
		{
			if (fillBlocks[i] == null) continue;

			if (fillBlocks[i].enabled)
			{
				neededBlocks.Add(fillBlocks[i]);
			}
			else
			{
				if (!Application.isPlaying)
				{
					DestroyImmediate(fillBlocks[i].gameObject);
				}
				else
				{
					Destroy(fillBlocks[i].gameObject);
				}
			}
			fillBlocks[i] = null;
		}
		fillBlocks = neededBlocks;
	}
	private struct CombinationCheck
	{
		public Vector3 startPoint;
		public Vector3 endPoint;
		public bool hit;

		public CombinationCheck(Vector3 startPoint, Vector3 endPoint, bool hit)
		{
			this.startPoint = startPoint;
			this.endPoint = endPoint;
			this.hit = hit;
		}
	}

	private BoxCollider CheckCombinable(BoxCollider start, Vector3 direction)
	{
		//Debug.Log("bounds: " + start.bounds + ", min: " + start.bounds.min + ", max: " + start.bounds.max);
		Vector3 center = start.bounds.center;
		// linecast in that direction
		Vector3 target = center + Vector3.Scale(direction, start.bounds.extents) + direction * 0.01f;
		BoxCollider result = null;

		// do linecast from center to target
		RaycastHit hitInfo;

		//float maxDistance = Vector3.Scale(direction, start.bounds.extents).magnitude + 0.1f;
		//Debug.Log("" + center.ToString() + " to " + target.ToString());

		//if (Physics.Raycast(center, direction, out hitInfo, maxDistance, fillLayerMask))
		if (Physics.Linecast(center, target, out hitInfo, fillLayerMask))
		{
			//Debug.Log("hit");
			// check the collider that is hit, if any
			Collider other = hitInfo.collider;
			Vector3 mySize = start.bounds.size;
			Vector3 otherSize = other.bounds.size;
			Vector3 scaler = new Vector3(
				1.0f - Mathf.Abs(direction.x),
				1.0f - Mathf.Abs(direction.y),
				1.0f - Mathf.Abs(direction.z)
			);

			mySize = Vector3.Scale(mySize, scaler);
			otherSize = Vector3.Scale(otherSize, scaler);

			if (mySize == otherSize)
			{
				// see if it has the same dimensions except for the direction
				// if so then return that collider
				result = other.GetComponent<BoxCollider>();
			}
			combinationChecks.Add(new CombinationCheck(center, target, true));
		}
		else
		{
			combinationChecks.Add(new CombinationCheck(center, target, false));
		}

		return result;
	}

}
